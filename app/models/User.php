<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');
    
    /*
     * Columnas llenables desde el constructor por medio de un arreglo
     */
    protected $fillable = array("name", "lastname", "address", "suburb", "email", "gender", "birthday", "password");
    
    /*
     * Configura una relacion uno a muchos de users-phones
     * Es decir, un usuario puede tener muchos telefonos
     * 
     */
    public function phones(){
        return $this->morphMany("Phone", "owner");
    }
    
    
    /**
     * Configura una relación uno a uno, con el de separar la información vital
     * de la información adicional.
     */
    public function userInfo()
    {
        return $this->hasOne('UserInfo');
    }
    
    /**
     * Configura una relación muchos a muchos, entre usuarios y roles
     */
    public function roles()
    {
        return $this->belongsToMany('Role');
    }
    

    /**
     * Get the unique identifier for the user
     * @return type
     */
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    /**
     * Get the password
     * @return type
     */
    public function getAuthPassword() {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail() {
        return $this->email;
    }

}

