<?php

class Phone extends Eloquent {
    
    protected $fillable=array("number","type");
    
    public function owner()
    {
        return $this->morphTo();
    }

}

?>
