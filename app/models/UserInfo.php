<?php

/**
 * Description of UserInfo
 *
 * @author Juan
 */
class UserInfo extends Eloquent {
    
    protected $fillable=array("address","suburb","image_url");
    
    public function user()
    {
        return $this->belongsTo('User');
    }
}

?>
