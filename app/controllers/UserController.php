<?php

/**
 * Controlador para el manejo de registro e ingreso de usuarios
 * @author Juan
 */
class UserController extends BaseController {
    // protected $layout = "users.signin.formulario";

    /**
     * Lleva al formulario de registro
     */
    public function getIndex() {
        return Redirect::to("users/signin");
        ;
    }

    /**
     * Cierra la sesión y redirige a login
     */
    public function getLogout() {
        Auth::logout();
        return Redirect::to('users/login');
    }

    /**
     * Muestra el formulario de registro
     */
    public function getSignin() {
        return View::make('users.signin.formulario');
    }

    /**
     * Guarda al usuario en la base de datos y envía un email con el código de
     * confirmación
     */
    public function postSignin() {
        $name = Input::get('name');
        $lastname = Input::get('lastname');
        $email = Input::get('email');
        $gender = Input::get('gender');
        //Genera un código aleatorio
        $password = substr(str_replace(array("/", ".", "\\"), "", Hash::make((string) rand(10, 999999))), 10, 20);
        //Crea un arreglo con las variables antes definidas, mediante compact
        $attributes = compact("name", "lastname", "email", "gender", "password");
        //Instancia un nuevo usuario
        $user = new User($attributes);
        //Guarda el usuario en la base de datos
        $user->save();
        //Envía un email a la dirección dada
        $subject = "Bienvenida";
        if ($gender === "male") {
            $subject = "Bienvenido";
        }
        Mail::send('emails.signin.welcome', $attributes, function($message) use($attributes, $subject, $name) {
                    $message->to($attributes["email"], $attributes["name"])->subject("$subject $name");
                });
        return "Se ha enviado un correo de confirmación a $email";
    }

    /**
     * Finaliza el registro del usuario en la base de datos
     * @return string
     */
    public function getFinishSignin() {
        $return = "";
        if (Input::has('code')) {
            $code = Input::get('code');
            if (strlen($code) == 20) {
                $user = User::where('password', '=', $code)->first();
                $name = $user->name;
                $id = $user->id;
                $gender = $user->gender;
                $arreglo = array("name" => $name, 'gender' => $gender, "id" => $id);
                return View::make('users.signin.finish')->with($arreglo);
            } elseif (strlen($code) > 20) {
                return "Probablemente ya has completado satisfactoriamente tu registro. 
                    Te invitamos a iniciar sesión en " . HTML::linkAction("UserController@getLogin", "este link");
            }
        } else {
            $return = "Has llegado acá por error";
        }

        return $return;
    }

    public function postFinishSignin() {
        $return = "Ha habido un error";
        if (Input::has("password")) {
            $clave = Input::get("password");
            $id = Input::get('id');
            $password = Hash::make($clave);
            $user = User::find($id);
            $user->password = $password;
            $name = $user->name;
            $email = $user->email;
            $attributes = compact("name", "email", "clave");

            Mail::send('emails.signin.credentials', $attributes, function($message) use($attributes, $clave, $name) {
                        $message->to($attributes["email"], $attributes["name"])->subject("No borrar - Sinexiones");
                    });
            $user->save();
            $return = "Gracias por finalizar tu registro , te hemos enviado un correo con tus datos";
        }

        return $return;
    }

    public function getLogin() {
        $retorno = "";
        if (Auth::check()) {
            $retorno = "Ya has sido logueado " . HTML::linkAction("UserController@getLogout", "Haz clic acá para salir");
        } else {
            $retorno = View::make('users.login.index');
        }
        return $retorno;
    }

    /**
     * Muestra el formulario de ingreso a la página
     * @return type
     */
    public function postLogin() {
        $email = Input::get("email");
        $password = Input::get("password");
        $userdata = array("email" => $email, "password" => $password);
        if (Auth::attempt($userdata)) {
            $name = Auth::user()->name;
            Auth::user()->birthday = "1990-10-02";
            Auth::user()->save();
            return "Te has logueado correctamente como $name";
        } else {
            return "debes loguearte $email $password";
        }
    }

    /**
     * Recuerda la contraseña
     */
    public function getRemindPassword() {
        $credentials = array('email' => Input::get('email'));

        return Password::remind($credentials, function($message, $user) {
                            $message->subject('Recordatorio de tu contraseña');
                        });
    }

    public function getPaswordReset() {
        return View::make('users.login.password_reset')->with('token', Input::get("token"));
    }

    /**
     * Valida si el email está registrado en la base de datos
     * @return string
     */
    public function getValidateEmail() {
        $value = Input::get('email');
        $response = User::where("email", "=", $value)->get()->count();
        if ($response > 0) {
            $response = "1";
        }
        return $response;
        return $value;
    }

    public function getPrueba1() {
        $juan = User::find(1);
        return $juan->phones;
    }

    

    public function getLoginPruebas() {
        $email = Input::get('email');
        $password = Input::get('password');
        $user = User::where('email', '=', $email)->first();
        $name = $user->name;
        $hashedPassword = $user->password;
        $return = "hola, $name, lamentablemente fallamos";
        $pass = Hash::make($password);
        if (Hash::check($password, $hashedPassword)) {
            $return = "coinciden $pass";
        }

        return $return;
    }

    /**
     * Método encargado de validar los campos del formulario de registro
     */
    public function getSigninValidation() {
        $arreglo = Input::all();
        return  $this->SigninValidation($arreglo);
    }

    /**
     * 
     * @param array $arreglo Arreglo con la información a validar en el formato
     * campo=>valor
     */
    private function signinValidation($arreglo) {
        $return = 1;
        if (is_array($arreglo)) {
            $reglas=array();
            foreach ($arreglo as $campo => $valor) {
                switch ($campo) {
                    case "name":
                    case "lastname":
                        $reglas[$campo] = "required|regex:/^([a-zñÑ áéíóúÁÉÍÓÚ])+$/i|max:40";
                        break;
                    case "email":
                        $reglas[$campo] = "required|email|max:200|unique:users";
                        break;
                    case "gender":
                        $reglas[$campo] = "required|in:male,female";
                        break;
                    default:
                        break;
                }
            }
            $validador=  Validator::make($arreglo,$reglas);
            if($validador->fails()){
                $return=$validador->failed();
            }
            
        } else {
            $return = 0;
        }
        return $return;
    }

}

?>
