<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersPhones extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_phones', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->integer('user_id')->unsigned();
                    $table->foreign('user_id')
                            ->references('id')->on('users')
                            ->onDelete('cascade')->onUpdate('cascade');
                    $table->string('number', '20');
                    $table->enum('type', array('cellphone', 'landline'));
                    $table->timestamps();
                    
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("users_phones");
	}

}
