<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersInfo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_info', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->integer('user_id')->unsigned();
                    $table->foreign('user_id')
                            ->references('id')->on('users')
                            ->onDelete('cascade')->onUpdate('cascade');
                    $table->string('address')->nullable();
                    $table->string('suburb')->nullable();
                    $table->string('image_url')->nullable();
                    $table->timestamps();
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("users_info");
	}

}
