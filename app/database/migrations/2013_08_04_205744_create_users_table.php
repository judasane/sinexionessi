<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->string('name', 40);
                    $table->string('lastname', 40);                    
                    $table->string('email')->unique();
                    $table->enum('gender', array('male', 'female'))->nullable();
                    $table->string('password')->nullable();
                    $table->timestamp('birthday')->nullable();
                    $table->timestamps();

                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop("users");
    }

}

