<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Generación de nueva contraseña</h2>

		<div>
                    Para crear una nueva contraseña, completa este formulario: 
                    {{HTML::linkAction("UserController@getPaswordReset", "finalizar registro",array("token"=>$token));}}
		</div>
	</body>
</html>