@extends("layout")
@section("body")
<h1>Hola de nuevo {{$name}}</h1>.
<p>Has completado exitosamente tu registro en nuestra base de datos.</p> 
<p>Y es necesario que tomes nota de tu contraseña, ya que ha sido encriptada en nuestra base de datos
    y ni nosotros la sabemos </p>
<p>Tu contraseña: {{$clave}}</p>
<p>Gracias por preferirnos</p>
@endsection