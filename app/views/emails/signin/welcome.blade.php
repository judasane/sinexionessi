@extends("layout")
@section("body")
<h1>Hola {{$name}}</h1>.
<p>Te has registrado en nuestra base de datos y para nosotros es un gusto tenerte como cliente. Por eso mismo te damos la bienvenida a nuestra familia Sinexiones y esperamos que esperes lo mejor de nosotros, pues es exactamente lo que queremos brindarte.</p> 
<p>Para completar el registro, por favor sigue el siguiente link:</p>
{{HTML::linkAction("UserController@getFinishSignin", "finalizar registro",array("code"=>$password));}}
@endsection