@extends('users.signin.layout')
@section('title')
Regístrate en Sinexiones
@endsection
@section('header')
Únete a Sinexiones
@endsection
@section('action')
signin
@endsection
@section('inputs')
<input class="entrada_registro" name="name" id="nombre" type="text" value="Nombre"/>
<div class="mensaje"></div>
<input class="entrada_registro" name="lastname" id="apellido" type="text" value="Apellido"/>
<div class="mensaje"></div>
<input class="entrada_registro" name="email" id="email" type="text" value="E-mail"/>
<div class="mensaje"></div>
<div class="radio">
    <input type="radio" id="gender_male" name="gender" value="male">
    <label for="gender_male" >Masculino</label>
    <input type="radio" id="gender_female" name="gender" value="female">
    <label for="gender_female" >Femenino</label>
</div>
<input class="entrada_registro" type="submit" value="Enviar" id="enviar"/>
@endsection