@extends('layout')

@section('title')
Entra a tu cuenta
@endsection

@section('resources')
{{HTML::style("css/estilos_principal.css")}}
{{HTML::style("css/registro.css")}}
@endsection

@section('body')
@yield('header_panel')
<div id="contenedor_cuerpo">
    <div id="contenedor_registro">
        <h1> @yield('header')</h1>
        <form id="formulario" action="@yield('action')" method="POST">
            <fieldset>
                
                @yield('inputs')
                
            </fieldset>
        </form>
    </div>
</div>
@yield('footer_principal')
@endsection
