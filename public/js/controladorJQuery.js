//controlador javascript para la validación del formulario
$(document).on("ready", function() {

    //Selecciona todos los elementos input de tipo texto y les asigna funciones en caso de blur y keyup
    var cajas = $(".caja");
    var formulario = $("#form_registro");
    var toggleButtons=$(".label_radio");

    var divRadio = $("#contenedor_radios");
    divRadio.focus();
    cajas.bind({
        blur: procederBlur
    });
    
    formulario.bind({
        submit:procederSubmit
    })
    
    toggleButtons.bind({
        click:validarToggleButton
    })
});